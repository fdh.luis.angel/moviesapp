# Movies app


[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Aplicación android para ver informacion de peliculas consumiendo la api: **TMDb**, programado enteramente en JAVA.

Capturas:

![](capture/capture1.png)
#
![](capture/capture2.png)
#
![](capture/MoviesGoDigital.gif)