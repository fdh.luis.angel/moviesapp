package com.udenar.moviesgodigital.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.fragment.FragmentNavigator;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.udenar.moviesgodigital.R;
import com.udenar.moviesgodigital.model.Movie;

import java.util.List;

import static com.udenar.moviesgodigital.utils.Contants.IMAGES_API_URL;

public class MovieAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<Movie> movies;
    private NavController navController;

    public MovieAdapter(Context context, List<Movie> movies, NavController navController) {
        this.context = context;
        this.movies = movies;
        this.navController = navController;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_grid_movie, parent, false);
        return new MovieAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder h, int position) {
        MovieAdapter.ViewHolder holder = (MovieAdapter.ViewHolder)h;
        final Movie movie = movies.get(position);

        holder.imgPoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navController.navigate(R.id.detailsFragment, movie.toBundle());
            }
        });

        Glide.with(context).load(IMAGES_API_URL+movie.getPoster_path()).into(holder.imgPoster);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    //---------------------------------------------------------------------------------------------- ViewHolder
    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imgPoster;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPoster = itemView.findViewById(R.id.imagePoster);
        }
    }
}
