package com.udenar.moviesgodigital;

import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.FragmentNavigator;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.udenar.moviesgodigital.model.Movie;

import static com.udenar.moviesgodigital.utils.Contants.IMAGES_API_URL;


public class DetailsFragment extends Fragment {
    private Movie movie;
    private CollapsingToolbarLayout toolbarLayout;
    private TextView txtSummary, txtReleaseDate, txtVoteCount;
    private RatingBar ratingBar;
    private ImageView imgPoster , imgPrincipal;

    public DetailsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbarLayout = view.findViewById(R.id.collapsing_toolbar);
        txtSummary = view.findViewById(R.id.txt_summary);
        txtReleaseDate = view.findViewById(R.id.txt_release_date);
        txtVoteCount = view.findViewById(R.id.txt_vote_count);
        ratingBar = view.findViewById(R.id.ratingBar);
        imgPoster = view.findViewById(R.id.img_details_poster);
        imgPrincipal = view.findViewById(R.id.img_details_principal);

        if (getArguments() != null) {
            movie = Movie.toMovie(getArguments());
            loadInfo();
        }
    }

    private void loadInfo(){
        Glide.with(getContext()).load(IMAGES_API_URL+movie.getPoster_path()).into(imgPoster);
        Glide.with(getContext()).load(IMAGES_API_URL+movie.getBackdrop_path()).into(imgPrincipal);

        toolbarLayout.setTitle(movie.getTitle());

        txtSummary.setText(movie.getOverview());
        txtReleaseDate.setText(movie.getRelease_date());
        txtVoteCount.setText(movie.getVote_count()+"");
        ratingBar.setRating(movie.getVote_average()/2.0f);

        startAnimations();
    }

    private void startAnimations(){
        imgPrincipal.setAlpha(0f);
        imgPrincipal.animate().setDuration(2000).alpha(1f).start();

        imgPrincipal.animate().scaleX(1.1f).scaleY(1.1f).setDuration(10000).start();
    }
}