package com.udenar.moviesgodigital;

import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.FragmentNavigator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.udenar.moviesgodigital.adapters.MovieAdapter;
import com.udenar.moviesgodigital.controller.Controller;
import com.udenar.moviesgodigital.model.Movie;
import com.udenar.moviesgodigital.view.ErrorConectionView;
import com.udenar.moviesgodigital.view.ServerErrorView;

import java.util.ArrayList;
import java.util.List;


public class ListFragment extends Fragment implements Controller.MovieCallbackListener {

    public static String TAG = "LIST_FRAGMENT";
    private Controller controller;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private MovieAdapter movieAdapter;
    private List<Movie> movies;
    private ErrorConectionView errorConectionView;
    private ServerErrorView serverErrorView;

    public ListFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.controller = new Controller(this, getContext());
        this.config(view);
        view.setAlpha(0f);
        view.animate().setDuration(1000).alpha(1f).start();
    }

    public void config(View view){
        recyclerView = view.findViewById(R.id.recycler_movies);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),3));

        final NavController navController = Navigation.findNavController(view);

        this.movies = new ArrayList<>();

        movieAdapter = new MovieAdapter(getContext(), movies, navController);
        recyclerView.setAdapter(movieAdapter);

        swipeRefreshLayout = view.findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onFetchStart();
            }
        });

        errorConectionView = view.findViewById(R.id.errorConectionView);
        serverErrorView = view.findViewById(R.id.serverConectionView);
        onFetchStart();
    }



    @Override
    public void onFetchStart() {
        errorConectionView.setVisibility(View.GONE);
        serverErrorView.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(true);
        controller.startfetching();
    }

    @Override
    public void onFetchProgress(Movie movie) {
        this.movies.add(movie);
        movieAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFetchProgress(List<Movie> movies) {
        recyclerView.setAlpha(0f);
        this.movies.clear();
        this.movies.addAll(movies);
        recyclerView.animate().alpha(1f).setDuration(3000).start();
        movieAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFetchComplete() {
        swipeRefreshLayout.setRefreshing(false);
        recyclerView.animate().alpha(1f).setDuration(1000).start();
    }

    @Override
    public void onErrorConection() {
        swipeRefreshLayout.setRefreshing(false);
        errorConectionView.setVisibility(View.VISIBLE);

        errorConectionView.setAlpha(0);
        errorConectionView.animate().setDuration(1000).alpha(1f).start();

        Toast.makeText(ListFragment.this.getContext(),"ERROR CONECTION", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onServerError(String error) {
        swipeRefreshLayout.setRefreshing(false);
        serverErrorView.setVisibility(View.VISIBLE);

        serverErrorView.setAlpha(0);
        serverErrorView.animate().setDuration(1000).alpha(1f).start();

        Toast.makeText(ListFragment.this.getContext(),"SERVER ERROR " + error, Toast.LENGTH_LONG).show();
    }
    //---------------------------------------------------------------------------------------------- CONTROLLER METHODS


    @Override
    public void onResume() {
        super.onResume();
        recyclerView.setAlpha(1f);
    }
}