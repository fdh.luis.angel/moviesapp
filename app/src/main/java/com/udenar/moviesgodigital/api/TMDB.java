package com.udenar.moviesgodigital.api;

import com.udenar.moviesgodigital.model.ModelMovies;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TMDB {

    @GET("movie/popular")
    Call<ModelMovies> getPopularMovies(@Query("api_key") String API_KEY);
}
