package com.udenar.moviesgodigital.controller;

import android.content.Context;
import android.util.Log;

import androidx.room.Room;

import com.udenar.moviesgodigital.api.TMDB;
import com.udenar.moviesgodigital.database.AppDatabase;
import com.udenar.moviesgodigital.model.ModelMovies;
import com.udenar.moviesgodigital.model.Movie;
import com.udenar.moviesgodigital.utils.APIUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.udenar.moviesgodigital.utils.Contants.API_KEY;

public class Controller {

    private static String TAG = "CONTROLLER";
    private MovieCallbackListener mListener;
    private TMDB tmdb;
    AppDatabase db;


    public Controller(MovieCallbackListener listener, Context context){
        mListener = listener;
        tmdb = APIUtils.getAPIService();

        db = Room.databaseBuilder(context,
                AppDatabase.class, "moviesdb").allowMainThreadQueries().build();
    }

    public void startfetching(){

        tmdb.getPopularMovies(API_KEY).enqueue(new Callback<ModelMovies>() {
            @Override
            public void onResponse(Call<ModelMovies> call, Response<ModelMovies> response) {
                if(response.isSuccessful()){
                    ModelMovies movies = response.body();
                    try {
                        db.movieDao().insert(movies.getResults());
                        mListener.onFetchProgress(movies.getResults());
                    }
                    catch (Exception e){
                        Log.e(TAG, "Error: ");
                        e.printStackTrace();
                    }
                    mListener.onFetchComplete();
                }
                else {
                    //Server 400 response
                    mListener.onServerError(response.code()+"");
                    Log.e(TAG, "NOT SUCCESSFUL: " + response);
                }
            }

            @Override
            public void onFailure(Call<ModelMovies> call, Throwable t) {
                //CONECTION ERROR
                Log.e(TAG, "onResponse FAIL ");
                t.printStackTrace();
                mListener.onErrorConection();
            }
        });


        db.movieDao().getAll().forEach(movie -> {
            mListener.onFetchProgress(movie);
        });
    }

    public interface MovieCallbackListener{
        void onFetchStart();
        void onFetchProgress(Movie movie);
        void onFetchProgress(List<Movie> movies);
        void onFetchComplete();
        void onErrorConection();
        void onServerError(String error);
    }
}
