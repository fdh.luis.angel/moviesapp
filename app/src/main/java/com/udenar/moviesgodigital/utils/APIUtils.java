package com.udenar.moviesgodigital.utils;

import com.udenar.moviesgodigital.api.TMDB;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import static com.udenar.moviesgodigital.utils.Contants.MOVIES_API_URL;

public class APIUtils {
    public static TMDB getAPIService(){
        return RetrofitClient.getClient(MOVIES_API_URL).create(TMDB.class);
    }

    public static class RetrofitClient{
        public static Retrofit retrofit = null;

        public static Retrofit getClient(String baseUrl){
            if(retrofit == null){
                retrofit = new Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            return retrofit;
        }
    }
}
