package com.udenar.moviesgodigital.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.udenar.moviesgodigital.model.Movie;
import com.udenar.moviesgodigital.model.MovieDao;

@Database(entities = {Movie.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract MovieDao movieDao();
}
