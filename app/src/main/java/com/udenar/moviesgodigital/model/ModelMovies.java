package com.udenar.moviesgodigital.model;

import java.util.List;

public class ModelMovies {
    int page;
    List<Movie> results;

    public ModelMovies() {
    }

    public ModelMovies(int page, List<Movie> results) {
        this.page = page;
        this.results = results;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<Movie> getResults() {
        return results;
    }

    public void setResults(List<Movie> results) {
        this.results = results;
    }
}
