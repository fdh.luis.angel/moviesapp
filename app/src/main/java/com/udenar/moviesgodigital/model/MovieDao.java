package com.udenar.moviesgodigital.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface MovieDao {
    @Query("SELECT * FROM MOVIE")
    public List<Movie> getAll();

    @Query("SELECT * FROM MOVIE WHERE id = :id")
    Movie getById(int id);

    @Insert
    void insert(List<Movie> movies);

    @Delete
    void delete(Movie movie);
}
