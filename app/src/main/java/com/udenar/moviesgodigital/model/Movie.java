package com.udenar.moviesgodigital.model;

import android.os.Bundle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Movie {
    boolean adult;
    String backdrop_path;
    @PrimaryKey
    int id;
    String original_language, original_title, overview;
    float popularity;
    String poster_path, release_date, title;
    boolean video;
    float vote_average;
    int vote_count;

    public Movie() {
    }

    public Movie(boolean adult, String backdrop_path, int id, String original_language, String original_title, String overview, float popularity, String poster_path, String release_date, String title, boolean video, float vote_average, int vote_count) {
        this.adult = adult;
        this.backdrop_path = backdrop_path;
        this.id = id;
        this.original_language = original_language;
        this.original_title = original_title;
        this.overview = overview;
        this.popularity = popularity;
        this.poster_path = poster_path;
        this.release_date = release_date;
        this.title = title;
        this.video = video;
        this.vote_average = vote_average;
        this.vote_count = vote_count;
    }

    public Bundle toBundle(){
        Bundle b = new Bundle();
        b.putBoolean("adult", this.adult);
        b.putString("backdrop_path", this.backdrop_path );
        b.putInt("id", this.id);
        b.putString("original_language", this.original_language);
        b.putString("original_title", this.original_title );
        b.putString("overview", this.overview );
        b.putFloat("popularity", this.popularity);
        b.putString("poster_path", this.poster_path );
        b.putString("release_date", this.release_date );
        b.putString("title", this.title );
        b.putBoolean("video", this.video );
        b.putFloat("vote_average", this.vote_average);
        b.putInt("vote_count", this.vote_count);
        return b;
    }

    public static Movie toMovie(Bundle b){
        Movie m = new Movie();
        m.setAdult(b.getBoolean("adult"));
        m.setBackdrop_path(b.getString("backdrop_path" ));
        m.setId(b.getInt("id"));
        m.setOriginal_language(b.getString("original_language"));
        m.setOriginal_title(b.getString("original_title"));
        m.setOverview(b.getString("overview"));
        m.setPopularity(b.getFloat("popularity"));
        m.setPoster_path(b.getString("poster_path" ));
        m.setRelease_date(b.getString("release_date" ));
        m.setTitle(b.getString("title"));
        m.setVideo(b.getBoolean("video"));
        m.setVote_average(b.getFloat("vote_average"));
        m.setVote_count(b.getInt("vote_count"));
        return m;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public String getOriginal_title() {
        return original_title;
    }

    public void setOriginal_title(String original_title) {
        this.original_title = original_title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public float getPopularity() {
        return popularity;
    }

    public void setPopularity(float popularity) {
        this.popularity = popularity;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isVideo() {
        return video;
    }

    public void setVideo(boolean video) {
        this.video = video;
    }

    public float getVote_average() {
        return vote_average;
    }

    public void setVote_average(float vote_average) {
        this.vote_average = vote_average;
    }

    public int getVote_count() {
        return vote_count;
    }

    public void setVote_count(int vote_count) {
        this.vote_count = vote_count;
    }
}
