package com.udenar.moviesgodigital.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.udenar.moviesgodigital.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}