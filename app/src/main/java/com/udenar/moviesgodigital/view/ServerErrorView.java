package com.udenar.moviesgodigital.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.udenar.moviesgodigital.R;

/**
 * TODO: document your custom view class.
 */
public class ServerErrorView extends ConstraintLayout {

    public ServerErrorView(@NonNull Context context) {
        super(context);
        initComponent(context);
    }

    public ServerErrorView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initComponent(context);
    }

    private void initComponent(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.server_error_view, null, false);
        this.addView(v);
    }
}
